import React, { Component } from 'react';
import learnButton from '../images/learn-button.png';
import workButton from '../images/work-button.png';

class Tile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      playVideo: false,
    };
  }

  render() {
    const { title, subTitle, text, bg, type, bold, textEnd, textStart } = this.props;
    let backgroundImage =  require(`../images/${bg}`)
  
    return (
      <div
        className="tile"
        style={{
          backgroundImage:
           'url(' +
            backgroundImage +
            ')',
        }}
      >
  
        <div className="info">
      
         <b><p>{textStart}{bold}{subTitle}{textEnd}</p> </b>
          <p>{text}</p>
        </div>
      {type !=='wharf' ?   <div className="options">
          <div className="play">
            <div onClick={() => this.props.onPlay(bg === 'scroll-image.jpg' ? 2 : 0 )}>
             <img style={{width:'89px'}} src={workButton} />
            </div>
      
          </div>
        </div> : null }
      
        <div className="options">
          <div onClick={() => this.props.onPlay(bg === 'scroll-image.jpg' ? 2 : 1)} className="learn">
          <img style={{width:'75px'}} src={learnButton} />
       
          </div>
        </div>
      </div>
    );
  }
}

export default Tile;
