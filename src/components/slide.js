import React, { Component } from 'react';
import CloseButton from './closeButton';
import PlayButton from './playButton';


class Slide extends Component {
  constructor(props) {
    super(props);
    let video = null;
    this.state = {
      shouldPlay: false,
      paused: false
    };
  }

  closeSlide() {
    this.setState({
      shouldPlay: false,
    });

    this.props.onClose();
  }

  componentWillMount(){
    this.setState({
      shouldPlay: false
    })
  }

  componentWillReceiveProps(newProps)
  {
    if(newProps.videoUrl !== this.props.videoUrl) {
      this.setState({
        shouldPlay: false
      })
    }
  }

  playVideo() {
    this.setState({ shouldPlay: true });
  }

  playNext(){
   this.props.playNext()
   this.setState({shouldPlay: false})
  }

  onNext(){
    this.setState({ shouldPlay: false });
   this.props.onNext()
  }

  onPrev(){
    this.setState({ shouldPlay: false });
    this.props.onPrev()
  }

  render() {
    console.log(imageUrl)
    const { prevText, nextText, imageUrl, noLast, noFirst, videoUrl } = this.props;
    //const image = require(`../images/${imageUrl}`);
    let vid = 'require(`../videos/${videoUrl}.mp4`);'

    if (this.state.shouldPlay) {
      //this.video.play()
    }
    return (
      <div
        style={{
          background: 'black',
          flex: 1,
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat',
          backgroundImage:
            'linear-gradient(0deg,rgba(0,0,0,0.3),rgba(0,0,0,0.3)),url(' +
            imageUrl +
            ')',
        }}
        className="video-wrapper"
      >
        <CloseButton onClose={() => this.closeSlide()} />
    
        {this.state.shouldPlay ? (
        
          <video
            style={{ objectFit: 'cover',    width: '100%',
            height: '100%'}}
            onPause={() => this.setState({paused: true})}
            onPlay={() => this.setState({paused: false})}
            controls  
            ref={element => (this.video = element)}
            key={vid} 
            autoPlay={true}
            onEnded={() => this.playNext()}
            id="background-video"
            autoPlay
          >
            <source src={vid} type="video/mp4" />
            Your browser does not support the video tag.
          </video>
       
        ) : null}

        <div style={noFirst ? {zIndex: -5} : {}} onClick={() => this.onPrev()} className="navigation left-side">
        <div  className="left arrow"> </div>
        <div className="text-wrap"> <p>{prevText && this.state.paused ? 'previous' : ''} <span>{prevText ? prevText : ''} </span> </p> </div>
        </div>
        
        <PlayButton onPlay={() => this.playVideo()} />

        <div style={noLast ? {zIndex: -5} : {}} className="navigation right-side" onClick={() => this.onNext()}>
           <div className="text-wrap"><p>{nextText && this.state.paused ? 'next' : ''} <span>{nextText  ? nextText : ''} </span> </p></div>
          <div  className="right arrow" />
        </div>
        <div className="previous" />
      </div>
    );
  }
}

export default Slide;
