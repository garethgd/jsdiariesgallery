import React from 'react';

const closeButton = props => (
  <div onClick={() => props.onClose()}>
    <div className="close-button-outer">
      <div className="close-button">x</div>
    </div>
  </div>
);

export default closeButton;
