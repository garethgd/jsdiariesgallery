import React from 'react';

const playButton = props => (
  <div onClick={() => props.onPlay()} class="play-button-outer">
    <div class="play-button" />
  </div>
);

export default playButton;
