import React, { Component } from 'react';
import Tile from '../components/tile';
import Slide from '../components/slide';
import * as contentful from 'contentful'

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      playVideo: false,
      images: [],
      videoId: '',
      videoUrl: '',
      images: [
        
      ],
      mediaIndex: 0
    };
  }

  componentWillMount() {
    var client = contentful.createClient({
      space: '0xfd7xoxbzdz',
      accessToken: '169cfc82a0e2322c28eef60076d03410153317c28b69f806d6666861339ea34d' })
    
      client.getEntries().then(entries => {
        entries.items.forEach(entry => {
          if(entry.fields) {
            console.log(entry.fields);
            this.setState({images: entry.fields})
          }
        })
      })
  }

  playSlide(index) {
    this.setState({ playVideo: true, mediaIndex:  index });
    var elem = document.body;

    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
      elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
      document.body.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
      elem.msRequestFullscreen();
    }
  }

  closeSlide() {
    this.setState({ playVideo: false, mediaIndex: 0 });
  }

  nextSlide() {
   if( this.state.mediaIndex < this.state.images.mainImages.length -1 )
   {
    this.setState({mediaIndex: this.state.mediaIndex + 1})

   }

   else{
    this.setState({ playVideo: false, mediaIndex: 0 });
  }
  }

  prevSlide() {
    if( this.state.mediaIndex > 0 ){
    this.setState({mediaIndex: this.state.mediaIndex - 1})

    }
    else{
      this.setState({ playVideo: false });
    }

    
  }

  render() {
    const mediaIndex = this.state.mediaIndex;
    const isOnFirstSlide  = this.state.mediaIndex === 0;
    const isOnLastSlide = this.state.images.mainImages ?  this.state.mediaIndex === this.state.images.mainImages.length -1 : false;
    return (
      <div style={{  background: 'red'}} className="shaw">
        {this.state.playVideo ? (
          <Slide
            onClose={() => this.closeSlide()}
            onNext={() => this.nextSlide() }
            onPrev={() => this.prevSlide()}
            playNext={() => this.nextVideo()}
            videoUrl={''}
            imageUrl={`https:${this.state.images.mainImages[mediaIndex].fields.file.url}`}
            nextText={!isOnLastSlide ? this.state.images.mainImages[mediaIndex + 1].fields.title : ''}
            noLast={isOnLastSlide}
            noFirst={isOnFirstSlide}
            prevText={mediaIndex > 0 ? this.state.images.mainImages[mediaIndex - 1].fields.title: '' }
          />
        ) : (
          <div  style={{flex: 1}}>
            <Tile
              bg={'home.jpg'}
              onPlay={(index) => this.playSlide(index)}
              subTitle={
                'CMS Single Page Application Slider'
              }
              text={'Treadle platens were printing presses operated by foot. They were ideal for jobbing work, such as small and quick jobs like leaflets or flyers, because of their compact size and ease of use'}
            />
           
          </div>
        )}
      </div>
    );
  }
}

export default Home;
